void detectaGrifos(){
  val_grifo1 = digitalRead(grifo1);
  val_grifo2 = digitalRead(grifo2);
  val_grifo3 = digitalRead(grifo3);
  
    if (configurarT1==false){
      Grifa (val_grifo1, jarraNueva1, electrovalvula1, pinCaudalimetro1, grifo1, tiraje1);
    }
    if (configurarT2==false){
      Grifa (val_grifo2, jarraNueva2, electrovalvula2, pinCaudalimetro2, grifo2, tiraje2);
    }
    if (configurarT3==false){
      Grifa (val_grifo3, jarraNueva3, electrovalvula3, pinCaudalimetro3, grifo3, tiraje3);
  }
}//fin detectagrifos

void Caudal(){//se puede optimizar
  if(confvalvula1==true){
      tiempoActual=millis();
      flowRatemls1=0;
      flowRatemlm1=0;
    if(tiempoActual-tiempoPrevio>=intervaloEvento){
      freq1=(counter1/(intervaloEvento/1000));
      flowRatemls1=freq1/factorK;
      flowRatemlm1=flowRatemls1*60;
      Serial.println("T1-CONF.t1.txt=\""+((String)flowRatemls1)+"\"");
      Serial.println("T1-CONF.t3.txt=\""+((String)flowRatemlm1)+"\"");
      counter1=0;
      tiempoPrevio =tiempoActual;
    }
  }else if (confvalvula2==true){
    tiempoActual=millis();
      flowRatemls2=0;
      flowRatemlm2=0;
    if(tiempoActual-tiempoPrevio>=intervaloEvento){
      freq2=(counter2/(intervaloEvento/1000));
      flowRatemls2=freq2/factorK;
      flowRatemlm2=flowRatemls2*60;
      Serial.println("T2-CONF.t1.txt=\""+((String)flowRatemls2)+"\"");
      Serial.println("T2-CONF.t3.txt=\""+((String)flowRatemlm2)+"\"");
      counter2=0;
      tiempoPrevio =tiempoActual;
    }
  }else if (confvalvula3==true){
    tiempoActual=millis();
      flowRatemls3=0;
      flowRatemlm3=0;
    if(tiempoActual-tiempoPrevio>=intervaloEvento){
      freq3=(counter3/(intervaloEvento/1000));
      flowRatemls3=freq3/factorK;
      flowRatemlm3=flowRatemls3*60;
      Serial.println("T3-CONF.t1.txt=\""+((String)flowRatemls3)+"\"");
      Serial.println("T3-CONF.t3.txt=\""+((String)flowRatemlm3)+"\"");
      counter3=0;
      tiempoPrevio =tiempoActual;
    }
  }
}

void Grifa (bool estadoGrifo, bool jarraNueva, const byte electrovalvula, const byte pinCaudalimetro, const byte grifo, bool tiraje){
if (pinCaudalimetro==2){ //Tirador1
  if (estadoGrifo==1 && tiraje==true && jarraNueva==true &&flanquito1==true){
  digitalWrite(electrovalvula, LOW);
      attachInterrupt(digitalPinToInterrupt(pinCaudalimetro), Flow1, RISING);
      tirando1=true;
    }else if(estadoGrifo==0){
      digitalWrite(electrovalvula, HIGH); 
      flanquito1=true;
      tirando1=false;
    }
}else if (pinCaudalimetro==3){ //Tirador2
    if (estadoGrifo==1 && tiraje==true && jarraNueva==true &&flanquito2==true){
    digitalWrite(electrovalvula, LOW);
    attachInterrupt(digitalPinToInterrupt(pinCaudalimetro), Flow2, RISING);
    tirando2=true;
  }else if(estadoGrifo==0){
    digitalWrite(electrovalvula, HIGH); 
    flanquito2=true;
    tirando2=false;
   }
  }else if (pinCaudalimetro==20){
    if (estadoGrifo==1 && tiraje==true && jarraNueva==true &&flanquito3==true){
    digitalWrite(electrovalvula, LOW);
    attachInterrupt(digitalPinToInterrupt(pinCaudalimetro), Flow3, RISING);
    tirando3=true;
  }else if(estadoGrifo==0){
    digitalWrite(electrovalvula, HIGH); 
    flanquito3=true;
    tirando3=false;
   }
  }
}

void FlowConf1(){
  counter1++;
}
void FlowConf2(){
  counter2++;
}

void FlowConf3(){
  counter3++;
}

void Flow1()
{
   counter1++;
   tirado1+=counter1;
   Serial.println("T1-P_TIRAJE.j0.val="+(String)volJarPorcentT1); 
   Serial.println("T1-P_TIRAJE.t2.txt=\""+(String)saldoJarT1+"\"");
}

void Flow2()
{
   counter2++; 
   tirado2+=counter2; 
   Serial.println("T2-P_TIRAJE.j0.val="+(String)volJarPorcentT2);
   Serial.println("T2-P_TIRAJE.t2.txt=\""+(String)saldoJarT2+"\"");
      
}

void Flow3()
{
   counter3++; 
   tirado3+=counter3;
   Serial.println("T3-P_TIRAJE.j0.val="+(String)volJarPorcentT3);
   Serial.println("T3-P_TIRAJE.t2.txt=\""+(String)saldoJarT3+"\"");
}
