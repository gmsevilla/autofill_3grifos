//AutoPullaso version buen hacer
//***CAMBIAR***//  //
//
// -Añadir gestion de informacion de la pantalla configuracion:  
//  Abrir/Cerrar Ev's, Visualizar y configurar caudal (L/m, L/s)
//  Posibilidad de editar Vol Jarra y Resolucion de pulsos de contaje
//
// -Añadir seguridad:
//  Disponibilidad de marca, lectura de mismo ticket en 2 tiradores
//  
//   //  //  //  //


///////REVISAR POR QUE SOLO FUNCIONA BIEN TIRADOR 1

//GRIFOS,VALVULAS Y CAUDALIMETROS//
 const byte grifo1=28;
 const byte grifo2=27;
 const byte grifo3=29;
 
 const byte electrovalvula1=7;
 const byte electrovalvula2=6;
 const byte electrovalvula3=5;
 
 bool val_grifo1 = 0;
 bool val_grifo2 = 0;
 bool val_grifo3 = 0;
 
 const byte pinCaudalimetro1=2;
 const byte pinCaudalimetro2=3;
 const byte pinCaudalimetro3=20;
 
  //TIRADOR1
   bool tiraje1=false;
   bool jarraNueva1=false;
   bool flanquito1=false;
   bool tirando1=false;
  //TIRADOR2
   bool tiraje2=false;
   bool jarraNueva2=false;
   bool flanquito2=false;
   bool tirando2=false;
  //TIRADOR3
   bool tiraje3=false;
   bool jarraNueva3=false;
   bool flanquito3=false;
   bool tirando3=false;
   
//CAUDALIMETRO//
  //TIRADOR1
   volatile double counter1; 
   unsigned long tirado1=0;
   unsigned long idlectura1=0;
   
  //TIRADOR2
   volatile double counter2;
   unsigned long tirado2=0;
   unsigned long idlectura2=0;
   
  //TIRADOR3
   volatile double counter3;
   unsigned long tirado3=0;   
   unsigned long idlectura3=0;
   
//SERIAL COM//
//LL
  char c;
  uint8_t longitud_tramaRecibo =14;
  byte * tramaRecibo= new byte[longitud_tramaRecibo];
  bool flanquillo=false;

//Estructura trama Recepcion serial
   struct tramaQr{
    byte dirCom;//Pantalla o Tirador
    byte scanerId;
    byte marca;
    byte cant;
    unsigned short vol;
    float prec;
    unsigned long idlectura;
  };
    tramaQr trama;
    
//VARS DE QR Y MOSTRAR EN PANTALLA
  //TIRADOR1
    byte numJarT1=0;
    unsigned short volJarT1=0;
    unsigned short volJarTotalT1=0;
    unsigned short volJarActualT1=0;
  //TIRADOR2
    byte numJarT2=0;
    unsigned short volJarT2=0;
    unsigned short volJarTotalT2=0;
    unsigned short volJarActualT2=0;
  //TIRADOR3
    byte numJarT3=0;
    unsigned short volJarT3=0;
    unsigned short volJarTotalT3=0;
    unsigned short volJarActualT3=0;


  //Calibracion caudal
  unsigned long pulsostamp=0;
   unsigned long tiempoActual=0;
   const unsigned long intervaloEvento=1000;
   unsigned long tiempoPrevio=0;
   const float factorK = 2.25;
   unsigned long freq1=0;
   unsigned long freq2=0;
   unsigned long freq3=0;
   //////
   float flowRatemls1=0.0;
   float flowRatemls2=0.0;
   float flowRatemls3=0.0;
   float flowRatemlm1=0.0;
   float flowRatemlm2=0.0;
   float flowRatemlm3=0.0;
    
   //T1 
   byte marcaConfT1=1;//1CRUZCAMPO//2RADLER//3SIN//4GRANRESERVA//5HEINEKEN
   unsigned long volConfT1=365;
   unsigned short ResConfT1=40;
   bool configurarT1=false;
   bool confvalvula1=false;
    unsigned int volJarTotalPulsosT1=0;
    byte volJarPorcentT1=0;
    float saldoJarT1=0;
    float saldoJar2T1=0;
    float saldoJarAnteriorT1=0;
    byte saldoPorcentT1=0;
   //T2 
   byte marcaConfT2=1;
   unsigned long volConfT2=365;
   unsigned short ResConfT2=40;
   bool configurarT2=false;
   bool confvalvula2=false;
    unsigned int volJarTotalPulsosT2=0;
    byte volJarPorcentT2=0;
    float saldoJarT2=0;
    float saldoJar2T2=0;
    float saldoJarAnteriorT2=0;
    byte saldoPorcentT2=0;
   //T3
   byte marcaConfT3=1;
   unsigned long volConfT3=365;
   unsigned short ResConfT3=40;
   bool configurarT3=false;
   bool confvalvula3=false;
    unsigned int volJarTotalPulsosT3=0;
    byte volJarPorcentT3=0;
    float saldoJarT3=0;
    float saldoJar2T3=0;
    float saldoJarAnteriorT3=0;
    byte saldoPorcentT3=0;

   String labelconfT1="Cruzcampo";
   String labelconfT2="Cruzcampo";
   String labelconfT3="Cruzcampo";
  
  
  void setup() {
    Serial.begin(250000);
  //GRIFOS
  //memset(tramaRecibo, 0xFE, longitud_tramaRecibo);
   pinMode(grifo1,INPUT);
   pinMode(grifo2,INPUT);
   pinMode(grifo3,INPUT);
   //pinMode(grifo3,INPUT);
   pinMode(electrovalvula1,OUTPUT);
   pinMode(electrovalvula2,OUTPUT);
   pinMode(electrovalvula3,OUTPUT);
   //pinMode(electrovalvula3,OUTPUT);
   digitalWrite(electrovalvula1, HIGH);//HIGH=APAGADA, LOW=ENCENDIDA
   digitalWrite(electrovalvula2, HIGH);
   digitalWrite(electrovalvula3, HIGH);
  //CAUDALIMETROS
    pinMode(pinCaudalimetro1, INPUT_PULLUP);
    pinMode(pinCaudalimetro2, INPUT_PULLUP);
    pinMode(pinCaudalimetro3, INPUT_PULLUP); 
     //memset(tramaRecibo, 0xFE, longitud_tramaRecibo);
    Serial.flush();
  }

void loop() {
serialReception(); //Gestion de informacion 
detectaGrifos();  //Gestion grifos y caudal
actualizarVolSaldo(); //Gestion de actualizacion de datos
Caudal();
}
